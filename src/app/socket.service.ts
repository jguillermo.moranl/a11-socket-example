import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { i18nMetaToJSDoc } from '@angular/compiler/src/render3/view/i18n/meta';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { io, Socket } from 'socket.io-client';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  socket: Socket;
  url: Readonly<string> = 'http://localhost:3000';

  constructor() {
    this.socket = io(this.url);
  }

  listenEvent = (eventName: string) => {
    return new Observable((obs) => {
      this.socket.on(eventName, (dataFromBack) => {
        obs.next(dataFromBack);
      });
    });
  };

  emitEvent = (eventName: string, data: any) => {
    this.socket.emit(eventName, data);
  };
}
