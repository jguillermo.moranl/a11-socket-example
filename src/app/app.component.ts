import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SocketService } from './socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'A11Socket';
  sub1: Subscription;
  sub2: Subscription;
  constructor(private _socketService: SocketService) {}

  ngOnInit(): void {
    this.sub1 = this._socketService
      .listenEvent('MESSAGE_FROM_BACK')
      .subscribe((data) => console.log(data));

    this.sub2 = this._socketService
      .listenEvent('otro_evento')
      .subscribe((data) => console.log(data));
  }
  ngOnDestroy(): void {
    if (this.sub1) {
      this.sub1.unsubscribe();
    }
    if (this.sub2) {
      this.sub2.unsubscribe();
    }
  }
  sendMessage = () => {
    this._socketService.emitEvent('SEND_FROM_FRONT', 'MENSAJE HIJO DE PUTA');
  };
}
